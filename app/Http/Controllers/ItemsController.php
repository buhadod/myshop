<?php

namespace App\Http\Controllers;

use App\Items;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Items::all();
        return view("item/index")->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
            $rules = array(
                'name'       => 'required',
                'price'      => 'required|numeric'
            );
            
            $validator = Validator::make(Input::all(), $rules);

        
            if ($validator->fails()) {
                return Redirect::to('item/create')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                
                $item = new Items();
                $item->name  = $request->name;
                $item->price  = $request->price;
                $item->save();
    
                return Redirect::to("item");
            }
            
           
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $data = Items::findorFail($id);
        return view("item/show")->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Items::findorFail($id);
        return view("item/edit")->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $item = Items::findorFail($id);

        $rules = array(
            'name'       => 'required',
            'price'      => 'required|numeric'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::to('item/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            
            $item->name  = $request->name;
            $item->price  = $request->price;
            $item->save();

            return Redirect::to("item");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Items::findorFail($id);
        $item->delete();
        return Redirect::back();
    }
}
