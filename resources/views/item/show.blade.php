<html>
<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>

<br/><br/><br/>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Shop</div>
                
                <div class="card-body">

               

                <table class="table table-hover table-bordered">
                    <tr>
                        <th>Name</th>
                        <td>{{$data->name}} </td>
                        
                    </tr>
                    
                    <tr>
                        <th>Price</th>
                        <td>{{$data->price}} </td>
                    </tr>
                   
                    
                </table>
               
               
               
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>