<html>
<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>

<br/><br/><br/>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Item</div>
                
                <div class="card-body">


                        <table class="table table-hover table-bordered">
                                        <tr>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th></th>
                                        </tr>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{$item->name}} </td>
                                            <td>{{$item->price}} </td>
                                            <td>
                                            
                                                <a href="{{ url('item/'.$item->id) }}" class="btn btn-primary a-btn-slide-text"> View </a>  
                                            
                                                <a href="{{ url('item/'.$item->id.'/edit') }}" class="btn btn-primary a-btn-slide-text"> Edit </a>  

                                                <form action="{{ url('item/'.$item->id) }}" method="POST" style="display:inline">
                                                @csrf
                                                {{method_field('DELETE')}}
                                                    <input href="#" type="submit" value="Delete" class="btn btn-primary a-btn-slide-text" onclick="return confirm('Are you sure you want to delete ?')"></a> 
                                                </form>
                                                
                                            </td>
                                        </tr>
                                        @endforeach
                        </table>

                        <div align="right">
                                <a href="{{ url('item/create') }} " class="btn btn-primary a-btn-slide-text">Create </a>
                        </div>


    </div>
    </div>
    </div>
    </div>
    </div>
</body>
</html>